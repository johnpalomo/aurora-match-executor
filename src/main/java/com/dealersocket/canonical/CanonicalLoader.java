package com.dealersocket.canonical;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dealersocket.canonical.api.CanonicalEntity;
import com.dealersocket.canonical.api.Entity;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import io.dropwizard.jackson.Jackson;
import io.dropwizard.lifecycle.Managed;

public class CanonicalLoader implements Managed {

	private static final Logger log = LoggerFactory.getLogger(CanonicalLoader.class);

	private int totalEntitiesProcessed = 0;
	private ObjectMapper mapper = Jackson.newObjectMapper();

	private final List<File> listOfFiles;

	private final ServiceConfiguration configuration;
	private final WebTarget target;

	public CanonicalLoader(ServiceConfiguration configuration, Client lambdaClient) throws IOException, SQLException {
		this.configuration = configuration;
		this.target = lambdaClient.target(configuration.getEndpoint());

		listOfFiles = Files.walk(Paths.get(configuration.getInputDir())).filter(Files::isRegularFile).map(Path::toFile)
				.collect(Collectors.toList());

		String jdbcConnectionUrl = String.format("jdbc:mysql://%s:%d/%s", configuration.getClusterName(),
				configuration.getPort(), configuration.getDatabaseName());

		log.info("using connection string: {}", jdbcConnectionUrl);
	}

	@Override
	public void start() throws Exception {
		load();
	}

	@Override
	public void stop() throws Exception {
	}

	private void load() throws Exception {
		long start = System.currentTimeMillis();
		for (File file : listOfFiles) {
			log.info("processing json input file: {}", file.getName());
			Gson gson = new GsonBuilder().create();
			try (JsonReader jsonReader = new JsonReader(
					new InputStreamReader(new FileInputStream(file.getAbsolutePath())))) {
				jsonReader.beginArray();
				while (jsonReader.hasNext()) {
					if (totalEntitiesProcessed % configuration.getBatchSize() == 0) {
						log.debug("parsed {} entities", totalEntitiesProcessed);
					}
					totalEntitiesProcessed++;
					CanonicalEntity canonicalEntity = new CanonicalEntity(gson.fromJson(jsonReader, Entity.class));
					String requestEntity = mapper.writeValueAsString(canonicalEntity);
					log.info("request entity {}", requestEntity);
					Response response = target.request().post(javax.ws.rs.client.Entity
							.entity(mapper.writeValueAsString(canonicalEntity), MediaType.APPLICATION_JSON));

					log.info("received response code [{}] for customerId [{}]", response.getStatus(),
							canonicalEntity.getCustomerId());
				}
			}
		}

		log.info(String.format("parsed [%d] with a total time of [%d] ms\n", totalEntitiesProcessed,
				System.currentTimeMillis() - start));
	}
}
