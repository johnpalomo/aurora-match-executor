package com.dealersocket.canonical;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.client.Client;

import org.hibernate.validator.constraints.NotEmpty;

import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.client.JerseyClientConfiguration;
import io.dropwizard.setup.Environment;

public class CanonicalService extends Application<ServiceConfiguration> {

	public static void main(String args[]) throws Exception {
		(new CanonicalService()).run(args);
	}

	@Override
	public void run(ServiceConfiguration configuration, Environment environment) throws Exception {
		final Client client  = new JerseyClientBuilder(environment).using(configuration.getJerseyClientConfiguration()).build(getName());
		environment.lifecycle().manage(new CanonicalLoader(configuration, client));
	}
}

class ServiceConfiguration extends Configuration {

	@Valid
	@NotNull
    private JerseyClientConfiguration jerseyClientConfiguration = new JerseyClientConfiguration(); 

	@NotEmpty
	private String inputDir;
	
	private String endpoint;

	@NotEmpty
	private String clusterName;

	@NotEmpty
	private String databaseName;

	@Min(0)
	@Max(Integer.MAX_VALUE)
	private int port;

	@Min(0)
	@Max(Integer.MAX_VALUE)
	private int batchSize;

	@NotEmpty
	private String dbUser;

	@NotEmpty
	private String dbPass;

	public String getInputDir() {
		return inputDir;
	}

	public void setInputDir(String inputDir) {
		this.inputDir = inputDir;
	}

	public String getClusterName() {
		return clusterName;
	}

	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	} 

	public String getDbUser() {
		return dbUser;
	}

	public void setDbUser(String dbUser) {
		this.dbUser = dbUser;
	}

	public String getDbPass() {
		return dbPass;
	}

	public void setDbPass(String dbPass) {
		this.dbPass = dbPass;
	}

	public int getBatchSize() {
		return batchSize;
	}

	public void setBatchSize(int batchSize) {
		this.batchSize = batchSize;
	}

	public JerseyClientConfiguration getJerseyClientConfiguration() {
		return jerseyClientConfiguration;
	}

	public void setJerseyClientConfiguration(JerseyClientConfiguration jerseyClientConfiguration) {
		this.jerseyClientConfiguration = jerseyClientConfiguration;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
}