package com.dealersocket.canonical.api;

public class Entity {

	private Integer iEntityId;
	private Integer iSiteId;
	private Integer tiEntityType;
	private String vchCompanyName;
	private String vchFirstName;
	private String vchMiddleName;
	private String vchLastName;
	private String vchAddress1;
	private String vchAddress2;
	private String vchAddress3;
	private String vchCity;
	private String chState;
	private String chCountryCode;
	private String vchPostCode;
	private String vchWorkNumber;
	private String vchMobileNumber;
	private String vchFaxNumber;
	private String vchOtherNumber;
	private String vchEmailAddress;
	private Integer iSourceId;
	private String vchExternalReference;
	private String vchUserDefined1;
	private String vchUserDefined2;
	private Integer bValidAddress;
	private Integer tiRecordStatus;
	private String vchSpecialNote1;
	private String vchCounty;
	private String vchExternalReference2;
	private Integer iPreferredContactType;
	private Integer iNeverContactType;
	private String MaternalLastName;
	private Ecr[] ECR;

	public int getiEntityId() {
		return iEntityId;
	}

	public void setiEntityId(int iEntityId) {
		this.iEntityId = iEntityId;
	}

	public int getiSiteId() {
		return iSiteId;
	}

	public void setiSiteId(int iSiteId) {
		this.iSiteId = iSiteId;
	}

	public int getTiEntityType() {
		return tiEntityType;
	}

	public void setTiEntityType(int tiEntityType) {
		this.tiEntityType = tiEntityType;
	}

	public String getVchCompanyName() {
		return vchCompanyName;
	}

	public void setVchCompanyName(String vchCompanyName) {
		this.vchCompanyName = vchCompanyName;
	}

	public String getVchFirstName() {
		return vchFirstName;
	}

	public void setVchFirstName(String vchFirstName) {
		this.vchFirstName = vchFirstName;
	}

	public String getVchMiddleName() {
		return vchMiddleName;
	}

	public void setVchMiddleName(String vchMiddleName) {
		this.vchMiddleName = vchMiddleName;
	}

	public String getVchLastName() {
		return vchLastName;
	}

	public void setVchLastName(String vchLastName) {
		this.vchLastName = vchLastName;
	}

	public String getVchAddress1() {
		return vchAddress1;
	}

	public void setVchAddress1(String vchAddress1) {
		this.vchAddress1 = vchAddress1;
	}

	public String getVchAddress2() {
		return vchAddress2;
	}

	public void setVchAddress2(String vchAddress2) {
		this.vchAddress2 = vchAddress2;
	}

	public String getVchAddress3() {
		return vchAddress3;
	}

	public void setVchAddress3(String vchAddress3) {
		this.vchAddress3 = vchAddress3;
	}

	public String getVchCity() {
		return vchCity;
	}

	public void setVchCity(String vchCity) {
		this.vchCity = vchCity;
	}

	public String getChState() {
		return chState;
	}

	public void setChState(String chState) {
		this.chState = chState;
	}

	public String getChCountryCode() {
		return chCountryCode;
	}

	public void setChCountryCode(String chCountryCode) {
		this.chCountryCode = chCountryCode;
	}

	public String getVchPostCode() {
		return vchPostCode;
	}

	public void setVchPostCode(String vchPostCode) {
		this.vchPostCode = vchPostCode;
	}

	public String getVchWorkNumber() {
		return vchWorkNumber;
	}

	public void setVchWorkNumber(String vchWorkNumber) {
		this.vchWorkNumber = vchWorkNumber;
	}

	public String getVchMobileNumber() {
		return vchMobileNumber;
	}

	public void setVchMobileNumber(String vchMobileNumber) {
		this.vchMobileNumber = vchMobileNumber;
	}

	public String getVchFaxNumber() {
		return vchFaxNumber;
	}

	public void setVchFaxNumber(String vchFaxNumber) {
		this.vchFaxNumber = vchFaxNumber;
	}

	public String getVchOtherNumber() {
		return vchOtherNumber;
	}

	public void setVchOtherNumber(String vchOtherNumber) {
		this.vchOtherNumber = vchOtherNumber;
	}

	public String getVchEmailAddress() {
		return vchEmailAddress;
	}

	public void setVchEmailAddress(String vchEmailAddress) {
		this.vchEmailAddress = vchEmailAddress;
	}

	public Integer getiSourceId() {
		return iSourceId;
	}

	public void setiSourceId(Integer iSourceId) {
		this.iSourceId = iSourceId;
	}

	public String getVchExternalReference() {
		return vchExternalReference;
	}

	public void setVchExternalReference(String vchExternalReference) {
		this.vchExternalReference = vchExternalReference;
	}

	public String getVchUserDefined1() {
		return vchUserDefined1;
	}

	public void setVchUserDefined1(String vchUserDefined1) {
		this.vchUserDefined1 = vchUserDefined1;
	}

	public String getVchUserDefined2() {
		return vchUserDefined2;
	}

	public void setVchUserDefined2(String vchUserDefined2) {
		this.vchUserDefined2 = vchUserDefined2;
	}

	public Integer getbValidAddress() {
		return bValidAddress;
	}

	public void setbValidAddress(Integer bValidAddress) {
		this.bValidAddress = bValidAddress;
	}

	public Integer getTiRecordStatus() {
		return tiRecordStatus;
	}

	public void setTiRecordStatus(Integer tiRecordStatus) {
		this.tiRecordStatus = tiRecordStatus;
	}

	public String getVchSpecialNote1() {
		return vchSpecialNote1;
	}

	public void setVchSpecialNote1(String vchSpecialNote1) {
		this.vchSpecialNote1 = vchSpecialNote1;
	}

	public String getVchCounty() {
		return vchCounty;
	}

	public void setVchCounty(String vchCounty) {
		this.vchCounty = vchCounty;
	}

	public String getVchExternalReference2() {
		return vchExternalReference2;
	}

	public void setVchExternalReference2(String vchExternalReference2) {
		this.vchExternalReference2 = vchExternalReference2;
	}

	public Integer getiPreferredContactType() {
		return iPreferredContactType;
	}

	public void setiPreferredContactType(Integer iPreferredContactType) {
		this.iPreferredContactType = iPreferredContactType;
	}

	public Integer getiNeverContactType() {
		return iNeverContactType;
	}

	public void setiNeverContactType(Integer iNeverContactType) {
		this.iNeverContactType = iNeverContactType;
	}

	public String getMaternalLastName() {
		return MaternalLastName;
	}

	public void setMaternalLastName(String maternalLastName) {
		MaternalLastName = maternalLastName;
	}

	public Ecr[] getECR() {
		return ECR;
	}

	public void setECR(Ecr[] eCR) {
		ECR = eCR;
	}
}

class Individual {
	private String vchDriversLicenseNo;
	private String dtBirthDate;

	public String getVchDriversLicenseNo() {
		return vchDriversLicenseNo;
	}

	public void setVchDriversLicenseNo(String vchDriversLicenseNo) {
		this.vchDriversLicenseNo = vchDriversLicenseNo;
	}

	public String getDtBirthDate() {
		return dtBirthDate;
	}

	public void setDtBirthDate(String dtBirthDate) {
		this.dtBirthDate = dtBirthDate;
	}

	public Integer getCreditAppId() {
		return CreditAppId;
	}

	public void setCreditAppId(Integer creditAppId) {
		CreditAppId = creditAppId;
	}

	public String getVchSSN() {
		return vchSSN;
	}

	public void setVchSSN(String vchSSN) {
		this.vchSSN = vchSSN;
	}

	Integer CreditAppId;
	String vchSSN;
}

class Ecr {

	private String vchExternalId;
	private String vchReferenceType;
	private Individual[] I;

	public String getVchExternalId() {
		return vchExternalId;
	}

	public void setVchExternalId(String vchExternalId) {
		this.vchExternalId = vchExternalId;
	}

	public String getVchReferenceType() {
		return vchReferenceType;
	}

	public void setVchReferenceType(String vchReferenceType) {
		this.vchReferenceType = vchReferenceType;
	}

	public Individual[] getI() {
		return I;
	}

	public void setI(Individual[] i) {
		I = i;
	}

}

