package com.dealersocket.canonical.api;

public class CanonicalEntity {
	//private String canonicalId;
	private int customerId;
	private int dealershipId;
	private String firstName;
	private String lastName;
	private String companyName;
	private String address;
	private String phone1;
	private String phone2;
	private String phone3;
	private String email;
	private boolean isCompany = false;

	public CanonicalEntity(Entity entity) {
	//	this.canonicalId = null;
		this.customerId = entity.getiEntityId();
		this.dealershipId = entity.getiSiteId();
		setFirstName(entity.getVchFirstName());
		this.lastName = entity.getVchLastName();
		this.companyName = entity.getVchCompanyName();
		this.address = entity.getVchAddress1();
		setPhone1(entity.getVchOtherNumber());
		setPhone2(entity.getVchMobileNumber());
		setPhone3(entity.getVchWorkNumber());
		this.email = entity.getVchEmailAddress();
	}

	/*public String getCanonicalId() {
		return canonicalId;
	}

	public void setCanonicalId(String canonicalId) {
		this.canonicalId = canonicalId;
	}*/

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getDealershipId() {
		return dealershipId;
	}

	public void setDealershipId(int dealershipId) {
		this.dealershipId = dealershipId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		if (firstName != null) {
			firstName = firstName.trim();
			if (firstName.length() > 3) {
				firstName = firstName.substring(0, 3);
			}
			this.firstName = firstName;
		}
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = getScrubbedPhone(phone1);
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = getScrubbedPhone(phone2);
	}

	public String getPhone3() {
		return phone3;
	}

	public void setPhone3(String phone3) {
		this.phone3 = getScrubbedPhone(phone3);
	}

	private String getScrubbedPhone(String phoneNumber) {
		String scrubbed = null;
		if (phoneNumber != null) {
			scrubbed = phoneNumber.trim();
			if (scrubbed.length() > 6 && scrubbed.length() > 13 ) {
				scrubbed = scrubbed.substring(6, phoneNumber.length());
			}
			if(scrubbed.length() > 7) {
				scrubbed = scrubbed.substring(0, 7);
			}
		}
		return scrubbed;
	} 

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean getIsCompany() {
		return isCompany;
	}

	public void setIsCompany(boolean isCompany) {
		this.isCompany = isCompany;
	}
}
