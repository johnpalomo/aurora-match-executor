#Bergstrom Batch Inserter

## How to Run
1.  Update the application.yml to reflect the values for your aurora cluster
2.  Update the run configurations in eclipse to have `server application.yml` in the program arguments.
3.  Run com.dealersocket.canonical.CanonicalService class.  You should see the application start up in the eclipse console.
